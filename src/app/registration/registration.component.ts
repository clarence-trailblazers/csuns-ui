import { Component, OnInit, Input } from '@angular/core';
import { UserServiceService } from '../services/user-service.service';

import { User } from '../models/User';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {


  userName: String = '';
  licensePlate: String = '';
  phoneNumber: String = '';
  userJSON: User = { id: 0, name: this.userName, licensePlate: this.licensePlate, phoneNumber: this.phoneNumber };

  responseData;

  onButtonClicked() {
    this.userServiceService.addUser(this.userJSON)
      .subscribe(data => {
        this.responseData = data;
      });
    console.log(` User id=${this.userName}`)
  }
  constructor(private userServiceService: UserServiceService) { }

  ngOnInit(): void {
  }



}
