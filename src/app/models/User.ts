export class User {

    id: number;
    name: String;
    licensePlate: String;
    phoneNumber: String;

}